FROM openjdk:8-jdk-alpine
#ARG JAR_FILE=target/*.jar
COPY target/*.jar app.jar
# COPY pom.xml
# "RUN mvn install
ENTRYPOINT ["java","-jar","/app.jar"]